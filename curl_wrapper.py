"""
There are a handful of times where the machine im working on does not allow me to install pip or packages, but it does have python on it.
I need away to wrap curl output so I can use python dictionary functions on it
"""
import subprocess
import json


def load_json_shell_response(func):
    """An annotation to wrap a function returning a list of strings to be run as a subprocess that overrides the return type to a dict.
    Args:
        func (function): The annotated function

    Returns:
        dict: the json returned by the subprocess as a dict
    """
    def func_wrapper(shell_command):
        """Loads an utf-8 encoded dictionary-like str into a dict.

        Args:
            shell_command (list): The shell command to be run that returns an encoded json object via stdout.
        """
        return json.loads(subprocess.run(shell_command, capture_output=True).stdout.decode('utf-8'))
    return load_json_shell_response


@load_json_shell_response
def example_usage(shell_command):
    """just something to show how this is used
    """
    return shell_command

# the example in practice
# what_in_the_world = example_usage(['cat', 'ex_json.json'])
# print(what_in_the_world)

#!/bin/python3
# REQUIREMENTS:
# - gpg and pass installed and initialized.
# - lpass cli installed and authenticated
# It tries to accomplish this sanely, but I still suggest validating the output and renaming rany password files you dont like
# also, read the logs, in cases where the lastpass information is unable to be parsed the entry is logged then split
# conversion appearence:
# lpass -> pass
# lpass.name/lpass.username content:{password}
import subprocess
from logging import error

# lpass_lines[0] is just the header
# lpass_lines[-1] is a blank line
lpass_lines = subprocess.run(['lpass', 'export', '--fields=name,username,password'], capture_output=True).stdout.decode('utf-8').split('\n')[1:-1]
lpass_sets = [pair.split(',') for pair in lpass_lines]

# create the identifier
for lpass_set in lpass_sets:
    try:
        identifier = '{}/{}'.format(lpass_set[0].strip(), lpass_set[1].strip())
        if identifier[-1] == '/':
            identifier = identifier[:-1]
        proc = subprocess.Popen(['pass', 'insert', identifier, '--echo'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        proc.communicate(lpass_set[2].encode('utf-8'))
    except IndexError:
        error('Unable to create pass entry for {}'.format(lpass_set[:-1]))
